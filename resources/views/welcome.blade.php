<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-default">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li>
                                <a href="{{ route('mensajes.create') }}">
                                    Contacto
                                </a>
                            </li>
                            
                            @if (auth()->check())
                                <li>
                                    <a href="{{ route('mensajes.index') }}">Mensajes</a>
                                </li>
                                @if (auth()->user()->hasRoles(['admin']))
                                    <li>
                                        <a href="{{ route('usuarios.index') }}">Usuarios</a>
                                    </li>
                                 @endif
                            @endif
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            @if (auth()->guest())
                                <li>
                                    <a href="/login">Login</a>
                                </li>
                            @else
                                <li>
                                    <a href="/logout">Cerrar sesión</a>
                                </li>
                                <li>
                                    <a href="/usuarios/{{ auth()->id() }}/edit">Mi cuenta</a>
                                </li>
                            @endif
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>
        <div class="container">
            <h1>Home</h1>
            @yield('content')
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/all.js') }}"></script>
    </body>
</html>
